
#include <iostream>
#include <string>

using namespace std;


class Player
{

public:

    string PlayerName = "none";
    int Score = 0;
};


int main()
{   
    Player* P = new Player;
    int Numbers = 0;
    cout << "Number Of Players = ";
    cin >> Numbers;
    Player* Arr = new Player[Numbers];

    for (int i = 0; i < Numbers; i++)
    {
        cout << "\n" << "Enter Player Name = "; cin >> Arr[i].PlayerName;
        cout << "Enter Yout Score = "; cin >> Arr[i].Score;
        cout << endl;
        
    };

    int i, j;
    for (i = 0; i < Numbers - 1; i++)
    {
        for (j = 0; j < Numbers - i - 1; j++)
        {
            if (Arr[j].Score < Arr[j + 1].Score)
                swap(Arr[j], Arr[j + 1]);
        }            
    }

    cout << "Results: " << "\t" << "\n";

    for (int i = 0; i < Numbers; i++)
    {
        cout << "\t" << Arr[i].PlayerName << ": " << Arr[i].Score << "\n";
    };
   
    delete[] Arr;
    Arr = nullptr;

    return 0;
}

